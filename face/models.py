from django.db import models

# Create your models here.
class educator_tb(models.Model):
    name=models.CharField(max_length=100,default='')
    email=models.CharField(max_length=100,default='')
    password=models.CharField(max_length=100,default='')
    mobile=models.CharField(max_length=100,default='')
    country=models.CharField(max_length=100,default='')
    state=models.CharField(max_length=100,default='')
    city=models.CharField(max_length=100,default='')
    status=models.CharField(max_length=100,default='0')
    image=models.ImageField(upload_to='files',default='0')
    user_type=models.CharField(max_length=100,default='0')