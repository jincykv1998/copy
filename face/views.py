import sys
from .models import educator_tb
import warnings
import numpy as np
from django.contrib.auth import logout
from django.http import HttpResponseRedirect,HttpResponse
from django.shortcuts import redirect
from django.contrib import messages
from django.utils.datastructures import MultiValueDictKeyError
from django.core.files.storage import FileSystemStorage
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import random

# import _pickle as cPickle

import joblib
if not sys.warnoptions:
    warnings.simplefilter("ignore")

from django.shortcuts import render
#loading model
with open("model/model2.pkl", "rb") as file:
    model = joblib.load(file)

# Create your views here.
def indexView(request):
    return render(request, "face/i.html")

def predictionView(request):
    if request.method == "POST":
        gender = request.POST["gender"]
        age = request.POST["age"]
        location = request.POST["location"]
        famsize = request.POST["famsize"]
        traveltime = request.POST["traveltime"]
        studytime = request.POST["studytime"]
        failures = request.POST["failures"]
        paid = request.POST["paid"]
        activities = request.POST["activities"]
        nursery = request.POST["nursery"]
        higher = request.POST["higher"]
        internet = request.POST["internet"]
        freetime = request.POST["freetime"]
        health = request.POST["health"]

        
        data = np.array([ gender, age, location, famsize, traveltime, studytime, failures, 
                            paid, activities, nursery, higher, internet,freetime, health]).reshape(-1,14)
        result = model.predict(data)[0]
        print(result) #debug
        context = {"result" : result}
        return render(request, "face/result.html", context)
    return render(request, "face/prediction.html")


# ======================================Educator===========================================================================
def reg(request):
    if request.method=="POST":
        name=request.POST["name"]
        email=request.POST["email"]
        password=request.POST["password"]
        mobile=request.POST["mobile"]
        country=request.POST["country"]
        state=request.POST["state"]
        city=request.POST["city"]
        image=request.FILES["image"]
        a=educator_tb(name=name,email=email,password=password,user_type="educator",mobile=mobile,country=country,state=state,city=city,image=image,status="Pending")            
        a.save()
        messages.success(request,'Registered Successfully')
        return render(request,'face/register.html')
    else:
        return render(request,'face/register.html')

def login(request):
    if request.method=="POST":
        email=request.POST["email"]
        print(email)
        password=request.POST["password"]
        print(password)
        chk=educator_tb.objects.filter(email=email,password=password,user_type="admin")
        print(chk)
        if chk:
            for x in chk:
                request.session['id']=x.id
                messages.success(request,'Successfully Logged in')
            return render(request,'admin/admin_home.html')
        else:
            messages.error(request,'Invalid Username or Password')
            return render(request,'face/login.html')
    else:
        return render(request,'face/login.html')


def logout(request):
    if request.session.has_key('id'):
        del request.session['id']
        logout(request)
        return HttpResponseRedirect('/')

def edu_login(request):
    if request.method=="POST":
        email=request.POST["email"]
        password=request.POST["password"]
        print(password)
        chk=educator_tb.objects.filter(email=email,password=password,status="Approved")
        print(chk)
        if chk:
            for x in chk:
                request.session['id']=x.id
                messages.success(request,'Successfully Logged in')

            return render(request,'educator/educator_home.html')
        else:
            print("-----------------------")
            messages.error(request,'Invalid Username or Password')

            return render(request,'educator/login.html')
    else:
        print("_______________________")
        return render(request,'educator/login.html')

def add_educator(request):
    var=educator_tb.objects.all().filter(status="Pending",user_type="educator")
    return render(request,'admin/add.html',{'var':var})
def approve_educators(request):
    var=educator_tb.objects.all().filter(status="Approved",user_type="educator")

    return render(request,'face/approved.html',{'var':var})

def delete_status(request):
    if request.method == 'POST':
        id_r        = request.POST.get('entry_id')
        educator_tb.objects.filter(id = id_r).delete()
        messages.error(request, 'Successfully deleted . ')
        return render(request,'face/add.html')

        # return redirect('add_educator')
def UpdateStatus(request):
    if request.method == 'POST':
        id_r        = request.POST.get('entry_id')
        educator_tb.objects.filter(id = id_r).update(status="Approved")
        messages.success(request, 'Successfully updated . ')
        return redirect('add_educator')

def profile(request):
    ii=request.session['id']
    var=educator_tb.objects.all().filter(id=ii)
    return render(request,'educator/profile.html',{'views':var})

def Update_Profile(request):
    ii=request.session['id']
    var=educator_tb.objects.all().filter(id=ii)
    return render(request,'educator/update.html',{'db':var})
def Update_Profile1(request):
    if request.method=="POST":
        ii=request.session['id']
        name=request.POST["name"]
        email_id=request.POST["email"]
        password=request.POST["password"]
        place=request.POST["city"]
        phone_no=request.POST["mobile"]
        try:
            image_r=request.FILES["img"]
            fs = FileSystemStorage()
            file = fs.save(image_r.name, image_r)
        except MultiValueDictKeyError:
            file=educator_tb.objects.get(id=ii).image
        educator_tb.objects.filter(id=ii).update(name=name,email=email_id,password=password,city=place,mobile=phone_no,image=file)
        return redirect('profile')
    else:
        return render(request,'educator/update.html')

