from django.urls import path
from face import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.indexView, name="home"),
    path('prediction', views.predictionView, name="prediction"),
    path('reg', views.reg, name='reg'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('edu_login', views.edu_login, name='edu_login'), 
    path('add_educator', views.add_educator, name='add_educator'), 
    path('approve_educators', views.approve_educators, name='approve_educators'), 
    path('delete_status', views.delete_status, name='delete_status'), 
    path('UpdateStatus', views.UpdateStatus, name='UpdateStatus'),
    path('profile', views.profile, name='profile'),
    path('Update_Profile', views.Update_Profile, name='Update_Profile'),
    path('Update_Profile1', views.Update_Profile1, name='Update_Profile1'),
    
]
if settings.DEBUG:
    urlpatterns+=static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
